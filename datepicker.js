// Datepicker inputs
const datepickerInputs = [...document.querySelectorAll(".datepicker")];

// Open datepicker when inputs are focused
for (datepickerInput of datepickerInputs) {
  datepickerInput.addEventListener("focus", (event) => {
    selectedInput = event.target;
    currentDisplayedDate = selectedInput.value
      ? moment(selectedInput.value, "DD-MM-YYYY")
      : moment();
    openDatepicker(selectedInput);
  });
}

// Datepicker display element
const datepickerCalendar = document.querySelector(".dp-datepicker");

// Datepicker navigation container
const datepickerMonthSection = document.querySelector(".dp-month");

// Datepicker elements to populate
const datepickerMonthDatesSection = document.querySelector(".dp-month-dates");
const datepickerMonthNameSection = document.querySelector(".dp-month-name");

// Setup current date, default displayed date,
// and selected input (null by default)
let currentDate = moment();
let currentDisplayedDate = moment("09-05-2020", "DD-MM-YYYY");
let selectedInput = null;

// Listen for clicks to increment selected month
datepickerMonthSection.addEventListener("click", (event) => {
  const targetClass = event.target.className;
  if (targetClass != "dp-month-back" && targetClass != "dp-month-forward")
    return;
  incrementDisplayMonth(targetClass);
});

// Listen for clicks to select a date
datepickerMonthDatesSection.addEventListener("click", (event) => {
  if (!event.target.classList.contains("dp-date")) return;
  confirmDate(event.target.dataset.date, selectedInput);
});

// Open/Close Datepicker
function openDatepicker(datepickerInput) {
  constructDatepicker(datepickerInput);
}
function closeDatepicker() {
  datepickerCalendar.classList.remove("active");
}

//
function constructDatepicker(datepickerInput) {
  if (datepickerInput.value || datepickerInput.value != "") {
    date = moment(datepickerInput.value, "DD-MM-YYYY");
  } else {
    date = moment();
  }

  populateDatepicker(date, datepickerInput);
}

function populateDatepicker(dateObj, datepickerInput) {
  // Clear the datepicker
  datepickerMonthNameSection.innerHTML = "";
  datepickerMonthDatesSection.innerHTML = "";
  // Add the month title
  datepickerMonthNameSection.innerHTML = dateObj.format("MMM YYYY");

  // The current specified date in the input
  const selectedDate = moment(datepickerInput.value, "DD-MM-YYYY");
  // Check if the displayed month has the selected date
  const monthHasSelectedDate = dateObj.month() == selectedDate.month();
  // Loop through first 7 days of displayed month for the day labels
  for (var i = 0; i < 7; i++) {
    const dayLabel = document.createElement("div");
    dayLabel.className = "dp-day";
    dateString = `${i + 1}-${dateObj.month() + 1}-${dateObj.year()}`;
    dayLabel.innerHTML = moment(dateString, "DD-MM-YYYY").format("dd");
    datepickerMonthDatesSection.appendChild(dayLabel);
  }
  // Loop through days in the displayed month
  for (var i = 0; i < dateObj.daysInMonth(); i++) {
    const day = document.createElement("div");
    day.className =
      monthHasSelectedDate && selectedDate.date() - 1 == i
        ? "dp-date selected"
        : "dp-date";
    day.dataset.date = `${i + 1}-${dateObj.month() + 1}-${dateObj.year()}`;
    day.innerHTML = (i + 1).toString();
    datepickerMonthDatesSection.appendChild(day);
  }
  // Show the datepicker
  datepickerCalendar.classList.add("active");
}

// When a specific date is clicked
function confirmDate(date, datepickerInput) {
  datepickerInput.value = date;
  datepickerCalendar.classList.remove("active");
}

// Increment displayed month (+/-)
function incrementDisplayMonth(targetClass) {
  if (targetClass == "dp-month-back") {
    currentDisplayedDate = currentDisplayedDate.subtract(1, "months");
  } else {
    currentDisplayedDate = currentDisplayedDate.add(1, "months");
  }

  populateDatepicker(currentDisplayedDate, selectedInput);
}
